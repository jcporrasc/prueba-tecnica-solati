<?php

require_once(dirname(__FILE__) . '/../Services/Connection.php');
require_once(dirname(__FILE__) . '/../Services/CreateUser.php');

if (isset($_GET['type']) && $_GET['type'] == 'GetUsers') {

    $connection = new Database();
    $result = $connection->select();
}

if (isset($_POST['type']) && $_POST['type'] == 'CreateUser') {

    $createUser = new CreateUser();
    $result = $createUser->createUser($_POST['name'], $_POST['email'], $_POST['password']);
}


echo $result;
