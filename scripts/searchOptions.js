//Carga y cambio de imagenes de la barra del buscador
$(document).ready(function() {
    // var urls = ['images/8.jpg', 'images/6.jpg', 'images/9.jpg', 'images/10.jpg', ];
    var urls = ['images/Castillo-min_bca.jpg', 'images/Dumbo-min_bca.jpg', 'images/DNY02LR_bca.jpg', 'images/Pluto-min_bca.jpg', ];


    var cout = 1;
    $('#main-search').css('background-image', 'url("' + urls[0] + '")');
    setInterval(function() {
        $('#main-search').css('background-image', 'url("' + urls[cout] + '")');
        cout == urls.length - 1 ? cout = 0 : cout++;
    }, 7440);
    document.querySelector('.js-icon').addEventListener("mouseenter", function(event) {
        document.querySelector('.js-activeTooltip').classList.add('active-tooltip')
    });
    document.querySelector('.js-icon').addEventListener("mouseout", function(event) {
        document.querySelector('.js-activeTooltip').classList.remove('active-tooltip')
    });
    document.querySelector('.js-icon-days').addEventListener("mouseenter", function(event) {
        document.querySelector('.js-activeTooltip-days').classList.add('active-tooltip-days')
    });
    document.querySelector('.js-icon-days').addEventListener("mouseout", function(event) {
        document.querySelector('.js-activeTooltip-days').classList.remove('active-tooltip-days')
    });
});


//Consumo API para precios de las opciones de los parques
$("body").on("click", "#search", function(e) {
    e.preventDefault();
    var $owl = $('.owl-carousel');
    $owl.trigger('destroy.owl.carousel');
    var fecha = $('#booking-date-search').val();
    var dias = $('#dias-search').val();
    var fechaInicio = moment(fecha, "MM-DD-YYYY").format("YYYY-MM-DD");
    var fechaFin = moment(fecha, "MM-DD-YYYY").add(parseInt(dias), 'days').format("YYYY-MM-DD");
    $('#fecha-option').html('<strong>' + fechaInicio + ' / ' + fechaFin + '</strong>');
    if ($('#adultos').val() == '0' || fecha == '' || dias == '0') {
        Swal.fire({
            icon: "warning",
            title: "<h2 style='padding-top:15px; padding-bottom:15px font-size:48px; margin-bottom:0px; color:#595959;'>Advertencia",
            cancelButtonColor: '#6e7d88',
            cancelButtonText: 'Cancelar',
            html: "<p>Por favor, complete todos los campos",
            iconColor: '#005EB8',
            showConfirmButton: false,
            showCancelButton: true,
            width: '500px'
        })
    } else {
        $('#options-loader').show();
        $('#options-result').hide();
        $("#options-items").html("");
        $('html,body').animate({
            scrollTop: $("#options-loader").offset().top
        }, 'slow');
        $.ajax({
            url: "./Controllers/ConsultaPreciosController.php",
            method: "POST",
            data: {
                paquetes: '1',
                adultos: $('#adultos').val(),
                niños: $('#niños').val(),
                fechaInicio: fechaInicio,
                fechaFin: fechaFin,
                dias: dias
            },
            dataType: 'JSON',
        }).done(function(response) {

            response.data.Adultos.forEach(function(element, index) {
                var formatter = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'USD',
                });
                var taza = $('#taza').val();

                var precioAdulto = parseFloat(element.amount.substr(element.amount.indexOf('$') + 1));
                var precioNiño = parseFloat(response.data.Niños[index].amount.substr(response.data.Niños[index].amount.indexOf('$') + 1));
                var precioFinal = precioAdulto + precioNiño;
                $("#options-items").append('<div class="carousel-item slick-current slick-active"><a class="carousel-item listing-item-container"><div class="listing-item"><img src="images/' + (index + 5) + '.jpg" alt=""><div class="listing-item-details"><ul></ul></div><div class="listing-item-content"><span class="tag"style="font-size:13px;font-size: 12px;font-weight: 600; background: #005EB8; border-radius: 0px 25px 25px 0px;"><strong>' + element.days + '</strong> Día(s)</span></div></div><div class="star-rating" data-option="' + (index + 1) + '" data-rating="3.5" style="font-size: 18px;"><span id="option" style="color: #005EB8; font-weight: 600; font-size: 20px;">' + element.option + '</span></br><span style="font-size:12px">Valor total adultos: <strong id="total-adultos">US ' + formatter.format(precioAdulto) + '</strong></span></br><span style="font-size:12px">Valor total niños: <strong id="total-niños">US ' + formatter.format(precioNiño) + '</strong></span><br><span style="font-size:13px; color: #005EB8">Valor total: <strong id="precioTotal-cart-' + (index + 1) + '" data-option="' + (index + 1) + '">US ' + formatter.format(precioFinal) + '</strong></span><br><span style="font-size:13px; color: #005EB8">Valor total: <strong id="precioTotal-cartCOP" data-option="' + (index + 1) + '" style="font-size:13px;">COP ' + formatter.format(precioFinal * taza) + '</strong></span></div><center><a id="confirm_cart" data-option="' + (index + 1) + '"class="button" style="min-width:46%; text-align:center; border-radius: 10px; background:#0CC588;">Comprar</a><a id="view-details" data-option="' + (index + 1) + '" class="button"style="width:46%; text-align:center; border-radius: 10px; background:#FFFFFF; box-shadow: inset 0 0 0 2px #6E7D88; color:#222222">Detalle</a></center></div>');
                $("#image").val("images/" + index + 5 + ".jpg");
            });
            $('#options-loader').hide();
            $('#total-oferta').html('<strong>$ ' + response.total + '</strong>');
            $("#options-result").removeAttr("style");
            $('html,body').animate({
                scrollTop: $("#options-result").offset().top
            }, 'slow');
            $(".owl-carousel").owlCarousel({
                lazyLoad: true,
                loop: true,
                nav: true,
                navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                margin: 10,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    992: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    }
                }
            });

        }).fail(function(responseErr) {
            $('#options-loader').hide();
        });
    }
});


//Consumo de API para ver más detalles de cada opción de parque
$("body").on("click", "#view-details", function(e) {
    $('#view-details').html('<i class="fa fa-refresh fa-spin"> ');
    $('#view-details').attr("disabled", true);
    var option = "";
    var obj = this;

    $(this).val(function() {
        option = $(this).attr("data-option");
        return $(this).attr("data-option");
    });

    if ($("#option-details-" + option).length) {
        $("#option-details-" + option).show();
        $(obj).attr("id", "view-less-details");
        $("#view-less-details[data-option='" + option + "']").html('Ver menos detalles <i class="fa fa-caret-up" aria-hidden="true"></i>');
    } else {
        console.log("Cargando");
        $.ajax({
            url: "./Controllers/ConsultaPreciosController.php",
            method: "POST",
            data: {
                desc: '1',
                option: option
            },
            dataType: 'JSON',
        }).done(function(response) {
            Swal.fire({
                title: "<h2 style='padding-top:30px; font-size:44px; margin-bottom:0px; color:#595959;'>Detalles del Paquete",
                // html: "<span>Antes de continuar necesitamos confirmar si le diste un vistazo a la disponibilidad en la pagina de <strong>Disney</strong> y que es posible asistir en las fechas que especificaste<br/>" + "<strong>" + fecha + "</strong>" + "<br/><br/>Puedes revisarlo dando <br/><a style='color: #005eb8' target='blank_' href='https://disneyworld.disney.go.com/availability-calendar/?segments=tickets,resort,passholder&defaultSegment=tickets'>Click aquí</a></span>",
                html: response.data[0].large,
                customClass: {
                    cancelButton: 'swal2-cancel',
                },
                showCancelButton: false,
                // cancelButtonColor: '#6e7d88',   
                confirmButtonColor: '#00c382',
                confirmButtonText: 'Ok!',
                width: '800px',
                focusConfirm: true,

            }).then((result) => {
                if (result.isConfirmed) {

                    $('#view-details').html('Detalle');
                    $('#view-details').attr("disabled", false);
                } else {
                    $('#view-details').html('Detalle');
                    $('#view-details').attr("disabled", false);

                }
            })

        }).fail(function(r) {
            $('#view-details').html('Detalle');
            $('#view-details').attr("disabled", false);
            console.log(r);
        });
    }

});

//Ver menos detalles en cada opción de disponibilidad
$("body").on("click", "#view-less-details", function(e) {

    var option = "";

    $(this).val(function() {
        option = $(this).attr("data-option");
        return $(this).attr("data-option");
    });

    $("#option-details-" + option).hide();
    $(this).attr("id", "view-details");
    $("#view-details[data-option='" + option + "']").html('Detalles <i class="fa fa-caret-down" aria-hidden="true"></i>');
});

//Consumo de API para mapeo de dias calendario
// $("#booking-date-search").change(function (e) {
//     e.preventDefault();
//     var fecha = $('#booking-date-search').val();
//     var fechaInicio = fecha.substr(0, fecha.indexOf('-'));
//     var fechaFin = fecha.substr(fecha.indexOf('-') + 1);
//     fechaInicio = moment(fechaInicio, "MM-DD-YYYY").format("YYYY-MM-DD");
//     fechaFin = moment(fechaFin, "MM-DD-YYYY").format("YYYY-MM-DD");
//     $.ajax({
//         url: "https://disneyworld.disney.go.com/availability-calendar/api/calendar?segment=tickets&startDate=" + fechaInicio + "&endDate=" + fechaFin,
//         method: "GET",
//         headers: {
//             'Accept': '*/*',
//             'Access-Control-Allow-Origin' : 'https://disneyworld.disney.go.com'
//         },
//         dataType:'jsonp'
//     })
// });