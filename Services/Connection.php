<?php

class Database
{
    protected $connection = null;

    public function __construct()
    {
        try {
            $this->connection = pg_connect("host=localhost port=5432 dbname=PruebaSolati user=postgres password=prueba123");

            if (!$this->connection) {
                throw new Exception("Error de conexion.");
            }
        } catch (Exception $error) {
            throw new Exception($error->getMessage());
        }
    }

    public function select($query = "")
    {
        try {
            if ($query == "") {

                $rows = $this->executeStatement($query);

                $response = [];
                while ($row = pg_fetch_row($rows)) {
                    $model = array(
                        "id" => $row[0],
                        "name" => $row[1],
                        "Password" => $row[2],
                        "Email" => $row[3],
                        "Status" => $row[4]
                    );
                    array_push($response, $model);
                }
                $response = json_encode($response);
                return $response;
            } else {
                $response = $this->executeStatement($query);
                return $response;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }

    private function executeStatement($query = "")
    {
        try {

            $generateConecction = $this->connection;
            if ($generateConecction === false) {
                throw new Exception("Error en preparacion de query: " . $query);
            }

            if ($query == "") {
                $result = pg_query($generateConecction, "SELECT * FROM public.users");
                return $result;
            } else {
                $result = pg_query($generateConecction, $query);
                return $result;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
