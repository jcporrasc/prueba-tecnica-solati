function GetUsers() {

    $.ajax({
        url: "./Controllers/HomeController.php",
        method: "GET",
        dataType: 'JSON',
        data: {
            type: 'GetUsers'
        }
    }).done(function (response) {
        console.log(response)
    }).fail(function (error) {
        console.log(error);
    });
}

function CreateUser() {
    var name = $("#name").val();
    var password = $("#password").val();
    var email = $("#email").val();

    $.ajax({
        url: "./Controllers/HomeController.php",
        method: "POST",
        dataType: 'JSON',
        data: {
            type: 'CreateUser',
            name: name,
            password: password,
            email: email
        }
    }).done(function (response) {
        console.log(response)
    }).fail(function (error) {
        console.log(error);
    });
}